import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:prod_app/pages/splash_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //set the orientation as portrait
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent, // transparent status bar
        statusBarIconBrightness: Brightness.dark, // status bar icons' color
      ),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          buttonColor: Colors.blue,
          textTheme: TextTheme(
            subtitle1: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold),
            button: TextStyle(color: Colors.white,fontSize: 14),
            bodyText1:TextStyle(color: Colors.black, fontSize: 14),
            bodyText2: TextStyle(color: Colors.black,fontSize: 13),
            overline: TextStyle(color: Colors.black, fontSize: 10),
            headline1: TextStyle(color: Colors.black,fontSize: 12,fontWeight: FontWeight.bold),
          ),
        ),
        home: SplashPage(),
      ),
    );
  }
}
