import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:prod_app/models/product_model.dart';
import 'package:prod_app/pages/product_details_page.dart';
import 'package:prod_app/repository/product_repository.dart';
import 'package:prod_app/utils/globals.dart' as globals;

class ProductListPage extends StatefulWidget {
  @override
  _ProductListPageState createState() => _ProductListPageState();
}

class _ProductListPageState extends State<ProductListPage> {
  ProductRepository _productRepository = ProductRepository();
  List<Product> _productsList = List<Product>();
  bool _isDataFetchingInProgress;

  @override
  void initState() {
    super.initState();
    _isDataFetchingInProgress = false;
    _getProducts();
  }

  _getProducts() async {
    _isDataFetchingInProgress = true;
    await _productRepository.getProductsList().then((value) {
      if (mounted) {
        setState(() {
          _productsList = value;
          _isDataFetchingInProgress = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: _isDataFetchingInProgress ? globals.loadingIndicator() : _displayProducts(),
    );
  }

  Widget _displayProducts() {
    return ListView.builder(
        itemCount: _productsList.length,
        itemBuilder: (_, index) {
          return Card(
            elevation: 3.0,
            child: ListTile(
              leading: _productsList[index].images.length > 0
                  ? globals.productImageContainer(_productsList[index].images[0].src, context)
                  : globals.productImageContainer('', context),
              title: Text(_productsList[index].name),
              subtitle: Text('Price : ${_productsList[index].price}'),
              trailing: globals.stockStatusContainer(_productsList[index].stockStatus, context),
              onTap: () {
                Navigator.push(
                    context,
                    PageTransition(
                        type: PageTransitionType.fade,
                        child: ProductDetailsPage(productId: _productsList[index].id)));
              },
            ),
          );
        });
  }
}
