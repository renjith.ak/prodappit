import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:prod_app/models/cart_model.dart';
import 'package:prod_app/pages/home_page.dart';
import 'package:prod_app/repository/user_repository.dart';
import 'package:prod_app/utils/globals.dart' as globals;
import 'package:prod_app/widgets/imageAssets.dart';
import 'package:prod_app/widgets/spacing.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  UserRepository _userRepository = UserRepository();
  var _formKey = GlobalKey<FormState>();
  final _userEmailController = TextEditingController();
  final _passwordController = TextEditingController();
  bool _passwordVisible, _isValidationInProgress;

  @override
  void initState() {
    super.initState();
    globals.cart = Cart();
    globals.cart.cartItems = List<CartItem>();
    _isValidationInProgress = false;
    _animationController =
        AnimationController(duration: const Duration(milliseconds: 500), vsync: this);
    _passwordVisible = false;
  }

  @override
  void dispose() {
    _userEmailController.dispose();
    _passwordController.dispose();
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _appLogo(),
                    _signInFormContainer(),
                    _loginButton(),
                    _poweredByContent(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _appLogo() {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          AppLogo(),
        ],
      ),
    );
  }

  Widget _signInFormContainer() {
    final Animation<double> offsetAnimation = Tween(begin: 0.0, end: 24.0)
        .chain(CurveTween(curve: Curves.elasticIn))
        .animate(_animationController)
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              _animationController.reverse();
            }
          });

    return Form(
      key: _formKey,
      child: Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            AnimatedBuilder(
                animation: offsetAnimation,
                builder: (buildContext, child) {
                  return Container(
                    padding: EdgeInsets.only(
                        left: offsetAnimation.value + 24.0, right: 24.0 - offsetAnimation.value),
                    child: TextFormField(
                      controller: _userEmailController,
                      keyboardType: TextInputType.emailAddress,
                      style: Theme.of(context).textTheme.bodyText2,
                      decoration: InputDecoration(
                        labelText: 'Email',
                        labelStyle: Theme.of(context).textTheme.bodyText1,
                        errorStyle: Theme.of(context).textTheme.overline,
                        prefixIcon: Icon(Icons.email),
                        enabledBorder:
                            UnderlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
                        focusedBorder:
                            UnderlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
                      ),
                      validator: (value) {
                        String text = validateEmail(value);
                        if (text != null) {
                          _animationController.forward(from: 0.0);
                          return text;
                        } else
                          return null;
                      },
                    ),
                  );
                }),
            VerticalSpacing(15),
            AnimatedBuilder(
                animation: offsetAnimation,
                builder: (buildContext, child) {
                  return Container(
                    padding: EdgeInsets.only(
                        left: offsetAnimation.value + 24.0, right: 24.0 - offsetAnimation.value),
                    child: TextFormField(
                      controller: _passwordController,
                      style: Theme.of(context).textTheme.bodyText2,
                      decoration: InputDecoration(
                        labelText: 'Password',
                        labelStyle: Theme.of(context).textTheme.bodyText1,
                        errorStyle: Theme.of(context).textTheme.overline,
                        enabledBorder:
                            UnderlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
                        focusedBorder:
                            UnderlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
                        prefixIcon: Icon(Icons.vpn_key),
                        suffixIcon: IconButton(
                          icon: _passwordVisible
                              ? Icon(Icons.visibility)
                              : Icon(Icons.visibility_off),
                          onPressed: () {
                            if (mounted) {
                              setState(() {
                                _passwordVisible = !_passwordVisible;
                              });
                            }
                          },
                        ),
                      ),
                      obscureText: !(_passwordVisible),
                      validator: (String value) {
                        if (value.isEmpty) {
                          _animationController.forward(from: 0.0);
                          return ('Password cannot be empty!');
                        } else
                          return null;
                      },
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Please Enter a Valid Email';
    else
      return null;
  }

  Widget _loginButton() {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          _isValidationInProgress ? CircularProgressIndicator() : SizedBox(),
          VerticalSpacing(20),
          ButtonTheme(
            minWidth: 250.0,
            child: RaisedButton(
              color: Theme.of(context).buttonColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: Text('LogIn', style: Theme.of(context).textTheme.button),
              onPressed: () async {
                //validate the entries
                if (!_formKey.currentState.validate()) return;

                //checking internet connectivity
                bool isNetConnected = await globals.checkInternetConnectivity();
                if (!isNetConnected) {
                  _scaffoldKey.currentState.showSnackBar(
                      globals.showSnackBar(message: globals.internetConnectivityMessage));
                  return;
                }
                if (mounted) {
                  setState(() {
                    _isValidationInProgress = true;
                  });
                }
                bool isValidated = await _userRepository.validateUser(
                    _userEmailController.text.trim(), _passwordController.text.trim());
                if (!isValidated) {
                  if (mounted) {
                    setState(() {
                      _isValidationInProgress = false;
                    });
                  }
                  _scaffoldKey.currentState
                      .showSnackBar(globals.showSnackBar(message: 'Invalid credentials!'));
                  return;
                }
                Future.delayed(Duration(seconds: 5), () {
                  if (mounted) {
                    setState(() {
                      _isValidationInProgress = false;
                    });
                  }
                  Navigator.push(context,
                      PageTransition(type: PageTransitionType.fade, child: HomePage(tabIndex: 0)));
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _poweredByContent() {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text('powered by', textAlign: TextAlign.center),
          CompanyNameImage(),
        ],
      ),
    );
  }
}
