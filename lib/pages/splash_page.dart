import 'dart:async';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:prod_app/pages/login_page.dart';
import 'package:prod_app/widgets/imageAssets.dart';

/*
This is the Splash Screen of the app, where the Logo only is displayed,
for 3 seconds and then proceed to the Login Page.
The delay of 3 seconds is customisable in the code below, as per the requirement.
*/

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    //creating a delay of 2 seconds
    Timer(const Duration(seconds: 2), onClose);
  }

  void onClose() {
    if (mounted) {
      Navigator.pushReplacement(
          context, PageTransition(type: PageTransitionType.fade, child: LoginPage()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(child: ProdAppTitleImage()),
        ],
      ),
    );
  }
}
