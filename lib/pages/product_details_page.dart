import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:page_transition/page_transition.dart';
import 'package:prod_app/models/cart_model.dart';
import 'package:prod_app/models/product_model.dart';
import 'package:prod_app/pages/home_page.dart';
import 'package:prod_app/repository/product_repository.dart';
import 'package:prod_app/utils/globals.dart' as globals;
import 'package:prod_app/widgets/spacing.dart';

class ProductDetailsPage extends StatefulWidget {
  final int productId;

  ProductDetailsPage({this.productId});

  @override
  _ProductDetailsPageState createState() => _ProductDetailsPageState();
}

class _ProductDetailsPageState extends State<ProductDetailsPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ProductRepository _productRepository = ProductRepository();
  Product _product = Product();
  bool _isDataFetchingInProgress;

  @override
  void initState() {
    super.initState();
    _isDataFetchingInProgress = false;
    _getProductDetails(widget.productId);
  }

  _getProductDetails(int productId) async {
    _isDataFetchingInProgress = true;
    await _productRepository.getProductDetails(productId).then((value) {
      if (mounted) {
        setState(() {
          _product = value;
          _isDataFetchingInProgress = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text('Product Details'),
            Text('${_product.name ?? ''}'),
          ],
        ),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add_shopping_cart,size: 30),onPressed: (){
            Navigator.push(context,
                PageTransition(type: PageTransitionType.fade, child: HomePage(tabIndex: 2)));
          }),
          HorizontalSpacing(10),
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: _isDataFetchingInProgress
            ? Center(child: globals.loadingIndicator())
            : ListView(
                children: <Widget>[
                  _productCard(),
                  VerticalSpacing(20),
                  _productDescription(),
                  VerticalSpacing(50),
                  _addToCartButton(),
                  VerticalSpacing(20),
                  _noteBelowText(),
                ],
              ),
      ),
    );
  }

  Widget _productCard() {
    return Card(
      elevation: 3.0,
      child: ListTile(
        leading: _product.images.length > 0
            ? globals.productImageContainer(_product.images[0].src, context)
            : globals.productImageContainer('', context),
        title: Text(_product.name),
        subtitle: Text('Price : ${_product.price}'),
        trailing: globals.stockStatusContainer(_product.stockStatus, context),
      ),
    );
  }

  Widget _productDescription() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Description:', style: Theme.of(context).textTheme.subtitle1),
        Html(
          data: _product.description,
          defaultTextStyle: Theme.of(context).textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _addToCartButton() {
    return Center(
      child: Container(
        height: 60.0,
        width: 330,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Add To Cart', style: Theme.of(context).textTheme.button),
              HorizontalSpacing(30),
              Icon(Icons.add_shopping_cart, color: Colors.white),
            ],
          ),
          onPressed: _product.stockStatus == StockStatus.OUTOFSTOCK
              ? null
              : () {
                  CartItem cartItem = CartItem();
                  cartItem.itemId = _product.id;
                  cartItem.itemName = _product.name;
                  _product.price = _product.price.trim().isEmpty ? '0' : _product.price;
                  cartItem.itemPrice = double.tryParse(_product.price);
                  cartItem.itemQuantity = 1;
                  cartItem.itemTotalPrice = cartItem.itemPrice * cartItem.itemQuantity;
                  _addItemToCart(cartItem);
                  //globals.cart.cartItems.add(cartItem);
                  _scaffoldKey.currentState.showSnackBar(
                      globals.showSnackBar(message: 'Item added to the cart successfully'));
                },
        ),
      ),
    );
  }

  _addItemToCart(CartItem cartItem) {
    bool itemAlreadyInList = false;
    for (int i = 0; i < globals.cart.cartItems.length; i++) {
      if (globals.cart.cartItems[i].itemId == cartItem.itemId) {
        itemAlreadyInList = true;
        globals.cart.cartItems[i].itemQuantity += 1;
        globals.cart.cartItems[i].itemTotalPrice += cartItem.itemTotalPrice;
      }
    }
    if (!itemAlreadyInList) globals.cart.cartItems.add(cartItem);
  }

  Widget _noteBelowText() {
    return _product.stockStatus == StockStatus.OUTOFSTOCK
        ? Text(
            '${globals.outofStockString}',
            style: Theme.of(context)
                .textTheme
                .overline
                .copyWith(color: Colors.lightGreen, fontStyle: FontStyle.italic),
          )
        : Text(
            '${globals.noteBelowString}',
            style: Theme.of(context)
                .textTheme
                .overline
                .copyWith(color: Colors.lightGreen, fontStyle: FontStyle.italic),
          );
  }
}
