import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:prod_app/models/product_model.dart';
import 'package:prod_app/pages/product_details_page.dart';
import 'package:prod_app/repository/product_repository.dart';
import 'package:prod_app/utils/globals.dart' as globals;
import 'package:prod_app/widgets/spacing.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController _textEditingController = TextEditingController();
  ProductRepository _productRepository = ProductRepository();
  List<Product> _productsList = List<Product>();
  List<Product> _filteredProductsList = List<Product>();
  bool _isDataFetchingInProgress;

  @override
  void initState() {
    super.initState();
    _textEditingController.text = '';
    _isDataFetchingInProgress = false;
    _getProducts();
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  _getProducts() async {
    _isDataFetchingInProgress = true;
    await _productRepository.getProductsList().then((value) {
      if (mounted) {
        setState(() {
          _productsList = value;
          _isDataFetchingInProgress = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          _searchContainer(),
          VerticalSpacing(20),
          _filteredProductsList.length == 0 ? _showNoItemsFoundText() : _displayFilteredProducts(),
        ],
      ),
    );
  }

  Widget _showNoItemsFoundText() {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('No Items found'),
        ],
      ),
    );
  }

  Widget _displayFilteredProducts() {
    return Expanded(
      child: ListView.builder(
          itemCount: _filteredProductsList.length,
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemBuilder: (_, index) {
            return Card(
              elevation: 3.0,
              child: ListTile(
                leading: _filteredProductsList[index].images.length > 0
                    ? globals.productImageContainer(
                        _filteredProductsList[index].images[0].src, context)
                    : globals.productImageContainer('', context),
                title: Text(_filteredProductsList[index].name),
                subtitle: Text('Price : ${_filteredProductsList[index].price}'),
                trailing:
                    globals.stockStatusContainer(_filteredProductsList[index].stockStatus, context),
                onTap: () {
                  Navigator.push(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: ProductDetailsPage(productId: _filteredProductsList[index].id),
                    ),
                  );
                },
              ),
            );
          }),
    );
  }

  Widget _searchContainer() {
    return Container(
      height: 80,
      decoration: BoxDecoration(color: Colors.blue, borderRadius: BorderRadius.circular(20)),
      child: Padding(
        padding: const EdgeInsets.only(left: 25, right: 25),
        child: Container(
          height: 50,
          alignment: Alignment.center,
          child: TextField(
            controller: _textEditingController,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(),
              //(above line)surprisingly, this is enough to have proper alignment
              fillColor: Colors.white,
              filled: true,
              border: InputBorder.none,
              prefixIcon: Icon(Icons.search, color: Colors.grey, size: 25.0),
              hintText: 'Search Product by Name',
              hintStyle: Theme.of(context).textTheme.bodyText2.copyWith(color: Colors.grey),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30.0),
                borderSide: BorderSide(color: Colors.white),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30.0),
                borderSide: BorderSide(color: Colors.white),
              ),
            ),
            onChanged: (value) {
              _filterProducts(value);
            },
          ),
        ),
      ),
    );
  }

  void _filterProducts(String value) {
    if (this._productsList.length == 0) return;
    _filteredProductsList.clear();
    if (value.trim().isNotEmpty) {
      _productsList.forEach((element) {
        if (element.name.toLowerCase().startsWith(value.trim().toLowerCase())) {
          _filteredProductsList.add(element);
        }
      });
      setState(() {
        _filteredProductsList = _filteredProductsList;
      });
    } else {
      setState(() {
        _filteredProductsList.clear();
      });
    }
  }
}
