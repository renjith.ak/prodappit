import 'package:flutter/material.dart';
import 'package:prod_app/pages/cart_page.dart';
import 'package:prod_app/pages/product_list_page.dart';
import 'package:prod_app/pages/search_page.dart';

class HomePage extends StatefulWidget {
  final int tabIndex;
  HomePage({this.tabIndex});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _tabController.index = widget.tabIndex;
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        bottom: TabBar(
          controller: _tabController,
          tabs: <Widget>[
            Tab(text: 'Home', icon: Icon(Icons.home,size: 40)),
            Tab(text: 'Search', icon: Icon(Icons.search,size: 40)),
            Tab(text: 'Cart', icon: Icon(Icons.add_shopping_cart,size: 40)),
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          ProductListPage(),
          SearchPage(),
          CartPage(),
        ],
      ),
      bottomNavigationBar: Material(
        color: Colors.yellow.shade800,
        child: TabBar(
          controller: _tabController,
          indicatorColor: Colors.red,
          indicatorWeight: 5,
          tabs: <Widget>[
            Tab(icon: Icon(Icons.home)),
            Tab(icon: Icon(Icons.search)),
            Tab(icon: Icon(Icons.add_shopping_cart)),
          ],
        ),
      ),
    );
  }
}
