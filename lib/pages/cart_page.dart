import 'package:flutter/material.dart';
import 'package:prod_app/models/cart_model.dart';
import 'package:prod_app/utils/globals.dart' as globals;
import 'package:prod_app/widgets/spacing.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  int _itemCount, _quantityCount;
  double _billAmount;

  @override
  void initState() {
    super.initState();
    _itemCount = 0;
    _quantityCount = 0;
    _billAmount = 0;
    _cartStatistics();
  }

  _cartStatistics() {
    _itemCount = 0;
    _quantityCount = 0;
    _billAmount = 0;
    _itemCount = globals.cart.cartItems.length;
    for (int i = 0; i < globals.cart.cartItems.length; i++) {
      _quantityCount += globals.cart.cartItems[i].itemQuantity;
      _billAmount += globals.cart.cartItems[i].itemTotalPrice;
    }
    setState(() {
      _itemCount = _itemCount;
      _quantityCount = _quantityCount;
      _billAmount = _billAmount;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      //height: MediaQuery.of(context).size.height,
      height: 300,
      child: globals.cart.cartItems.length == 0
          ? Center(child: Text('There are no items in the Cart'))
          : ListView(
              children: <Widget>[
                _cartNutShellContainer(),
                VerticalSpacing(20),
                _cartItemsData(),
              ],
            ),
    );
  }

  Widget _cartNutShellContainer() {
    return Container(
      height: 125,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.black, width: 1),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: Row(
          children: <Widget>[
            Container(
              height: 125,
              width: 125,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.blue,
                //border: Border.all(color: Colors.black,width: 1),
                borderRadius: BorderRadius.circular(20),
              ),
              //child: Icon(Icons.shopping_cart, color: Colors.white, size: 100),
              child: Stack(
                alignment: Alignment.topRight,
                children: <Widget>[
                  Icon(Icons.shopping_cart, color: Colors.white, size: 100),
                  Positioned(
                    child: Container(
                      width: 30,
                      height: 30,
                      alignment: Alignment.center,
                      decoration:
                      BoxDecoration(color: Colors.deepOrange, shape: BoxShape.circle),
                      child: Text('$_quantityCount',
                          style: TextStyle(color: Colors.white, fontSize: 12)),
                    ),
                  ),
                ],
              ),
            ),
            HorizontalSpacing(10),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text('Item Count:', style: Theme.of(context).textTheme.headline1),
                Text('Qty Count:', style: Theme.of(context).textTheme.headline1),
                Text('Bill Amnt:', style: Theme.of(context).textTheme.headline6),
              ],
            ),
            HorizontalSpacing(5),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('$_itemCount', style: Theme.of(context).textTheme.headline1),
                Text('$_quantityCount', style: Theme.of(context).textTheme.headline1),
                Text('$_billAmount', style: Theme.of(context).textTheme.headline6),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _cartItemsData() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        columnSpacing: 5.0,
        headingRowHeight: 20.0,
        horizontalMargin: 5.0,
        dividerThickness: 2.0,
        columns: <DataColumn>[
          DataColumn(
              label: Text('Item Name', style: Theme.of(context).textTheme.headline1),
              numeric: false),
          DataColumn(
              label: Text('Unit Price', style: Theme.of(context).textTheme.headline1),
              numeric: true),
          DataColumn(
              label: Text('       Quantity', style: Theme.of(context).textTheme.headline1),
              numeric: false),
          DataColumn(
              label: Text('Tot.Price', style: Theme.of(context).textTheme.headline1),
              numeric: false),
          DataColumn(label: Text(''), numeric: false),
        ],
        rows: globals.cart.cartItems
            .map((item) => DataRow(cells: [
                  DataCell(Container(width: 100.0, child: Text(item.itemName))),
                  DataCell(Center(child: Text('${item.itemPrice}'))),
                  DataCell(Row(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.remove_circle_outline),
                        onPressed: () {
                          if (item.itemQuantity == 1) {
                            confirmAndRemoveItem(item);
                          } else {
                            setState(() {
                              item.itemQuantity -= 1;
                              item.itemTotalPrice -= item.itemPrice;
                              _cartStatistics();
                            });
                          }
                        },
                      ),
                      Container(width: 18.0, child: Text('${item.itemQuantity}')),
                      IconButton(
                        icon: Icon(Icons.add_circle_outline),
                        onPressed: () {
                          setState(() {
                            item.itemQuantity += 1;
                            item.itemTotalPrice += item.itemPrice;
                            _cartStatistics();
                          });
                        },
                      ),
                    ],
                  )),
                  DataCell(Container(
                      width: 50.0,
                      child: Text(
                        '${item.itemTotalPrice}',
                        textAlign: TextAlign.right,
                      ))),
                  DataCell(removeItem(item)),
                ]))
            .toList(),
      ),
    );
  }

  Widget removeItem(CartItem cartItem) {
    return GestureDetector(
      child: Icon(Icons.close),
      onTap: () {
        confirmAndRemoveItem(cartItem);
      },
    );
  }

  confirmAndRemoveItem(CartItem cartItem) {
    var alert = AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20.0)),
      ),
      //backgroundColor: Colors.blueGrey,
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Confirm'),
          Divider(color: Colors.blueAccent),
        ],
      ),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text('Are you sure, you want to remove this item from the list?'),
          SizedBox(
            height: 15.0,
          ),
          yesNoButtonsForItemRemove(cartItem),
        ],
      ),
    );
    showDialog(context: context, builder: (context) => alert);
  }

  Widget yesNoButtonsForItemRemove(CartItem cartItem) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        RaisedButton(
          child: Text('Yes', style: Theme.of(context).textTheme.button),
          onPressed: () async {
            Navigator.pop(context, true);
            setState(() {
              //globals.cart.cartItems.removeAt(itemId);
              globals.cart.cartItems.remove(cartItem);
              _cartStatistics();
            });
          },
          color: Theme.of(context).primaryColorDark,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        ),
        RaisedButton(
          color: Theme.of(context).primaryColorDark,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: Text('No', style: Theme.of(context).textTheme.button),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
      ],
    );
  }
}
