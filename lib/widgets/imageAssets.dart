import 'package:flutter/material.dart';

class ProdAppTitleImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage('images/logo.jpg');
    Image image = Image(image: assetImage);
    return Container(child: image);
  }
}

class CompanyNameImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage('images/name.png');
    Image image = Image(image: assetImage);
    return Container(child: image);
  }
}

class AppLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage('images/logo.jpg');
    Image image = Image(image: assetImage);
    return Container(child: image);
  }
}