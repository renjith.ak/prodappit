import 'package:flutter/material.dart';

class HorizontalSpacing extends SizedBox{
  HorizontalSpacing(double value):super(width:value);
}

class VerticalSpacing extends SizedBox {
  VerticalSpacing(double value):super(height:value);
}
