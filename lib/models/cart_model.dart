class Cart {
  int cartId;
  double totalAmount;
  int itemCount;
  int quantityCount;
  List<CartItem> cartItems;

  Cart({this.cartId, this.totalAmount, this.itemCount, this.quantityCount,this.cartItems});
}

class CartItem {
  int itemId;
  String itemName;
  double itemPrice;
  int itemQuantity;
  double itemTotalPrice;

  CartItem({this.itemId, this.itemName, this.itemPrice, this.itemQuantity, this.itemTotalPrice});
}
