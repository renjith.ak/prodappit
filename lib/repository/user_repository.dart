abstract class UserRepositoryAbstract {
  Future<bool> validateUser(String userName, String password);
}

class UserRepository implements UserRepositoryAbstract {
  @override
  Future<bool> validateUser(String userEmail, String password) async {
    bool isValidated = false;
    try {
      if (userEmail == 'admin@abc.com' && password == '1234') {
        isValidated = true;
      } else {
        isValidated = false;
      }
    } catch (e) {
      isValidated = false;
      print('Inside catch of validateUser in user_repository.dart');
    }
    return isValidated;
  }
}
