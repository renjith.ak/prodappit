import 'dart:convert';
import 'package:prod_app/models/product_model.dart';
import 'package:prod_app/utils/globals.dart' as globals;
import 'package:http/http.dart' as http;

abstract class ProductRepositoryAbstract {
  Future<List<Product>> getProductsList();
  Future<Product> getProductDetails(int productId);
}

class ProductRepository implements ProductRepositoryAbstract {
  @override
  Future<Product> getProductDetails(int productId) async{
    Product _product = Product();
    try {
      String url = '${globals.endPointURL}/products/$productId';
      var uri = Uri.parse(url);
      uri = uri.replace(query: 'consumer_key=${globals.consumerKey}&consumer_secret=${globals.consumerSecret}');
      final response = await http.get(uri, headers: _setHeaders());
      if (response.statusCode == 200) {
        var decodedData = json.decode(response.body);
        _product = Product.fromJson(decodedData);
      }
    } catch (e) {
      print('Inside catch of getProductDetails(productId) in product_repository:${e.toString()}');
    }
    return _product;
  }

  @override
  Future<List<Product>> getProductsList() async{
    List<Product> _productsList = List<Product>();
    try {
      String url = '${globals.endPointURL}/products';
      var uri = Uri.parse(url);
      uri = uri.replace(query: 'consumer_key=${globals.consumerKey}&consumer_secret=${globals.consumerSecret}');
      final response = await http.get(uri, headers: _setHeaders());
      if (response.statusCode == 200) {
        _productsList = productFromJson(response.body);
      }
    } catch (e) {
      print('Inside catch of getProductsList() in product_repository:${e.toString()}');
    }
    return _productsList;
  }

  _setHeaders() => {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  };
}
