import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:prod_app/models/cart_model.dart';
import 'package:prod_app/models/product_model.dart';
import 'package:prod_app/widgets/spacing.dart';

String endPointURL = 'http://seedsbag.com/wp-json/wc/v3';
String consumerKey = 'ck_17f41c87bd388326ca04feb42e9a74154c6154c0';
String consumerSecret = 'cs_8d35e70c5c68930a0697018c181077bec3a1e054';

String internetConnectivityMessage = 'You are not connected to the Internet!';
String noteBelowString = '#When \'Add To Cart\' button is pressed, a quantity of 1 shall be added'
    ' by default. The user can increase/decrease the quantity in the \'View Cart\' page';

String outofStockString = '\'Add To Cart\' button is disabled as the item is Out of Stock.';
Cart cart = Cart();

Future<bool> checkInternetConnectivity() async {
  bool connectivity = false;
  ConnectivityResult result = await Connectivity().checkConnectivity();
  if (result == ConnectivityResult.none) {
    connectivity = false;
  } else {
    connectivity = true;
  }
  return connectivity;
}

SnackBar showSnackBar({String message, int duration: 2}) {
  final snackBar = SnackBar(
    content: Text(message),
    backgroundColor: Colors.blueAccent,
    duration: Duration(seconds: duration),
  );

  return snackBar;
}

Widget loadingIndicator() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      CircularProgressIndicator(),
      VerticalSpacing(10),
      Text('Fetching data...'),
    ],
  );
}

Widget stockStatusContainer(StockStatus stockStatus, BuildContext context) {
  return ClipOval(
    child: Container(
      height: 30,
      width: 100,
      color: stockStatus == StockStatus.INSTOCK ? Colors.blue : Colors.red,
      child: Center(
          child: stockStatus == StockStatus.INSTOCK
              ? Text('In Stock', style: Theme.of(context).textTheme.button)
              : Text('Out of Stock', style: Theme.of(context).textTheme.button)),
    ),
  );
}

Widget productImageContainer(String imageUrl, BuildContext context) {
  return Container(
    height: 70,
    width: 70,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.grey,
        border: Border.all(
          color: Colors.grey,
        )),
    child: imageUrl != ''
        ? loadImageFromNetwork(imageUrl)
        : Center(
            child: Text(
              'No Image',
              style: Theme.of(context).textTheme.button,
              textAlign: TextAlign.center,
            ),
          ),
  );
}

loadImageFromNetwork(String imgUrl) {
  return Image.network(
    imgUrl,
    //fit: BoxFit.cover,
    fit: BoxFit.contain,
    loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent loadingProgress) {
      if (loadingProgress == null) return child;
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
          strokeWidth: 2.0,
          value: loadingProgress.expectedTotalBytes != null
              ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
              : null,
        ),
      );
    },
  );
}
